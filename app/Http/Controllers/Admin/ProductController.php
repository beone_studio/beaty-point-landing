<?php

namespace App\Http\Controllers\Admin;

use App\Service;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{

    public function index(Request $request)
	{
        return Service::all();
    }
    
    public function getProduct($id)
	{
        return Service::where('id', $id)->first();
	}

    public function create(Request $request) 
    {
        $service = Service::create([
            'name' => $request->input('name'),
            'price' => $request->input('price'), 
            'time' => $request->input('time')
        ]);

		return ['result' => 'succsess'];
    }

	public function update($id, Request $request)
    {    
        $data = [
            'name' => $request->input('name'),
            'price' => $request->input('price'), 
            'time' => $request->input('time')
        ];

        $service = Service::where('id', $id)->first();

        $service->update($data);

        return ['result' => 'success'];
    }

    public function delete($id)
    {
        $service=Service::where('id', $id)->first();

        $service->delete();

        return ['result' => 'success'];
    }
}