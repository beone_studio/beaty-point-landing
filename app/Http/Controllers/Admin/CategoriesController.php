<?php 

namespace App\Http\Controllers\Admin;

use App\Style;

use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;
use App\StylesImages;
use App\StylesFeatures;

class CategoriesController extends Controller {

    public function getAllCategories() {
        return Style::orderBy('show_on_main', 'DESC')->orderBy('created_at', 'DESC')->get();
    }

    public function getCategory($id) {
        return Style::with('gallery', 'features')->where('id', $id)->first();
    }

	public function create(Request $request) {
        $data = [
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'about' => $request->input('about'),
            'short' => $request->input('short'),
            'show_on_main' => $request->input('show_on_main'),
        ];
        if ($request->hasFile('image')) {
            $data['image'] = $request->image->store('styles', 'public');
        }
        Style::create($data);
		return ['result' => 'success'];
	}

	public function update(Style $category, Request $request)
    {
        $data = [
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'about' => $request->input('about'),
            'short' => $request->input('short'),
            'show_on_main' => $request->input('show_on_main'),
        ];
        if ($request->hasFile('image')) {
            \Storage::disk('public')->delete($category->image);
            $data['image'] = $request->image->store('styles', 'public');
        }        
        $category->update($data);
        return ['result' => 'success'];
    }	

	public function delete($id)
    {
    	$category=Style::where('id', $id)->first();
        $category->delete();
        return ['result' => 'success'];
    }	

    public function addImage($id, Request $request)
    {
        $product = StylesImages::create([
            'alt' => $request->input('alt'),
            'order' => $request->input('order'), 
            'style_id' => $id,
            'image' => $request->image->store('style' . $id, 'public')
        ]);

		return ['result' => 'succsess'];
    }
    
    public function deletePhoto($id)
    {
        $photo=StylesImages::where('id', $id)->first();
        \Storage::disk('public')->delete($photo->image);
        $photo->delete();

        return ['result' => 'success'];
    }

    public function createFeature(Request $request)
    {
        $feature = StylesFeatures::create([
            'text' => $request->input('text'),
            'style_id' => $request->input('style_id'),
            'order' => $request->input('order'),
        ]);

        return ['result' => 'success'];
    }

    public function deleteFeature($id)
    {
        $feature = StylesFeatures::where('id', $id)->first();

        $feature->delete();

        return ['result' => 'success'];
    }
}