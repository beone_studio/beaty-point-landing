<?php

namespace App\Http\Controllers\Index;

use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function index($page = 'index')
    {
        if (view()->exists($view = 'index.' . $page)) {
            if ($page === 'index'){
                $services = Service::all();
                return view($view, compact('services'));
            }
        } else if ($page === 'admin'){
            return view('admin.login', [
                'error' => session('error')
            ]);
        } else {
            abort(404);
        }
    }
}
