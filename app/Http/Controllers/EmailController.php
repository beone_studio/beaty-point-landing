<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{

    public function send(Request $request){
        $product_name = $request->input('product', '');
        $title = ('Новая заявка' . $product_name);
        $name = $request->input('name', '');
        $email = $request->input('email', '');
        $text = $request->input('text', '');

        Mail::send(
            'emails.order',
            [
                'title' => $title,
                'name' => $name,
                'email' => $email,
                'text' => $text
            ],
            function ($message) use ($title)
            {

                $message->from('beone.by.mail@gmail.com', $title);

                $message->to('a.kinolider@gmail.com')->subject($title);

            }
        );

        return redirect('/')->with('flash_message', 'Ваша заявка принята');
    }
}
