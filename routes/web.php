<?php

Route::post('/send', 'EmailController@send');

Route::group(['namespace' => 'Index'], function () {
    Route::get('/{page?}', 'PageController@index');
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/', 'AuthController@login');
    Route::post('/', 'AuthController@doLogin');

    Route::group(['middleware' => 'auth'], function () {
        Route::group(['prefix' => 'api'], function () {
            Route::get('/logout', 'AuthController@logout');
            Route::post('/features', 'CategoriesController@createFeature');
            Route::get('/allCategories', 'CategoriesController@getAllCategories');
            Route::delete('/features/{id}', 'CategoriesController@deleteFeature');
            Route::group(['prefix' => 'categories'], function () {
                Route::get('/', 'CategoriesController@getAllCategories');
                Route::get('/{category}', 'CategoriesController@getCategory');
                Route::delete('/{id}', 'CategoriesController@delete');
                Route::post('/', 'CategoriesController@create');
                Route::post('/{category}', 'CategoriesController@update');
                Route::post('/{category}/image', 'CategoriesController@addImage');
                Route::delete('/image/{id}', 'CategoriesController@deletePhoto');
            });
            Route::group(['prefix' => 'products'], function () {
                Route::get('/', 'ProductController@index');
                Route::get('/{product}', 'ProductController@getProduct');
                Route::post('/', 'ProductController@create');
                Route::post('/{product}/image', 'ProductController@addImage');
                Route::post('/{product}', 'ProductController@update');
                Route::delete('/{product}', 'ProductController@delete');
                Route::delete('/image/{id}', 'ProductController@deletePhoto');
            });
        });

        Route::any('{all?}', function () {
            return view('admin.main');
        })->where(['all' => '.*']);
    });
});
