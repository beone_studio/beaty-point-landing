<!DOCTYPE HTML>
<html>
	<head>
		<title>Beauty Point | Минск</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="/css/app.css" />
		<link rel="stylesheet" href="/js/slider/skippr.css">
		<noscript><link rel="stylesheet" href="/css/noscript.css" /></noscript>
		<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    	<script src="/js/slider/skippr.js"></script>
	</head>
	<body class="is-preload">

		<!-- Page Wrapper -->
			<div id="page-wrapper">

				<!-- Header -->
					<header id="header" class="alt">
						<h1><a href="index.html"></a></h1>
						<nav>
							<a href="">+375 (29) 655-26-26</a>
							<a href="#menu">Меню</a>
						</nav>
					</header>

				<!-- Menu -->
					<nav id="menu">
						<div class="inner">
							<h2>Меню</h2>
							<ul class="links">
								<li><a href="">Немного о нас</a></li>
								<li><a href="">Услуги</a></li>
								<li><a href="#">Контакты</a></li>
							</ul>
						</div>
						<div class="hide">
							<img src="/images/right.png" alt="">
						</div>
						<a href="#" class="close">Закрыть</a>
					</nav>

				<!-- Banner -->
					<section id="banner">
						<div class="inner">
							<div class="logo"><span class="icon fa-diamond"></span></div>
							<h2>Beauty Point Studio</h2>
							<p>ПАРИКМАХЕРСКИЕ УСЛУГИ | ЭСТЕТИЧЕСКАЯ КОСМЕТОЛОГИЯ | ПЕРМАНЕНТНЫЙ МАКИЯЖ</p>
						</div>
					</section>

				<!-- Wrapper -->
					<section id="wrapper">

						<!-- One -->
							<section id="one" class="wrapper alt spotlight style2">
								<div class="inner">
									<div class="content">
										<h2 class="major">Европейское качество</h2>
										<p>Индивидуальный подход, лучшие расходные материалы и только высококлассное качество. Мы знаем, насколько важна гармоничная атмосфера салона для полного расслабления посетителей.
									</div>
								</div>
							</section>

						<!-- Two -->
							<section id="two" class="wrapper alt style3">
								<div class="inner">
									<div class="content">
										<h2 class="major">Услуги для мужчин</h2>
										<p>
											Навсегда канули в лету те времена, когда мужчина мог оставаться успешным и достигать высот абсолютно не обращая внимание на внешний вид.
											Именно поэтому мы предлагаем целый комплекс услуг для мужчин в наших салонах. Модные мужские стрижки, окрашивание волос, укладка, камуфлирование седины, маникюр и педикюр - всё это для вас и по доступным ценам в салонe «Beaty Point»!
										</p>
									</div>
								</div>
							</section>

						<!-- Three -->
							<section id="three" class="wrapper alt spotlight style2">
								<div class="inner">
									<div class="content">
										<h2 class="major">Услуги для мужчин</h2>
										<p>
											Навсегда канули в лету те времена, когда мужчина мог оставаться успешным и достигать высот абсолютно не обращая внимание на внешний вид.
											Именно поэтому мы предлагаем целый комплекс услуг для мужчин в наших салонах. Модные мужские стрижки, окрашивание волос, укладка, камуфлирование седины, маникюр и педикюр - всё это для вас и по доступным ценам в салонe «Beaty Point»!
										</p>
									</div>
								</div>
							</section>

							<section id="four" class="wrapper style1">
								<div class="inner">
									<h2 class="major">Список услуг</h2>
									<p>Высокие традиции безупречного обслуживания — один из главных факторов, влияющих на любовь и признание постоянных клиентов. Собственный неповторимый стиль оттачивался годами: салон красоты открылся более 18 лет назад, став законодателем стандартов и тенденций в столичной индустрии красоты.</p>
									<section class="features">
										<div class="table-wrapper">
											<table>
												<thead>
													<tr>
														<th>Название</th>
														<th>Длительность</th>
														<th>Цена (BYN)</th>
													</tr>
												</thead>
												<tbody>
													@foreach ($services as $service)
														<tr>
															<td>{{ $service->name }}</td>
															<td>{{ $service->time }}</td>
															<td>{{ $service->price }}</td>
														</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</section>
								</div>
							</section>

						<!-- Four -->
							<section id="five" class="wrapper alt style2">
								<div class="inner">
									<div class="content">
										<h2 class="major">Отзывы наших клиентов</h2>
										<div class="sliderContainer">
											<div id="theTarget">
												<div>
													<div class="align-block">
														<p>Хочу выразить огромную благодарность администрации салона за то, что предоставили мне возможность посетить сегодня моего любимого Мастера Елену Нечай!!!! Леночке — отдельное спасибо и низкий поклон ща чуткость, понимание и профессионализм!!! Леночка!!! Ты — лучшая!!! Огромное спасибо администрации салона за душевную атмосферу в вашем салоне!!!</p>
														<h4>Валерия Иванова</h4>
													</div>
												</div>
												<div>
													<div class="align-block">
														<p>
															Это лучший салон, в котором когда-либо была. Качество услуг на высочайшем уровне, администраторы и персонал приятны в общении. Делала окрашивание волос: быстро и качественно. Мастер по маникюру Любовь К. - ЛУЧШАЯ, у кого когда-либо была. Она делает просто произведения искусства. Влюбилась в мастерство сотрудников "Бонжур" с первого раза! Спасибо ВАМ.
														</p>
														<h4>Ирина Петрова</h4>
													</div>
												</div>
												<div>
													<div class="align-block">
														<p>
															Безумно порадовала косметика. Великолепный салон, прекрасное обслуживание, самое приятное — это спа-процедуры. Очень рада, что в нашем городе присутствуют такие салоны.
														</p>
														<h4>Евгения Петрова</h4>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>

					</section>





				<!-- Footer -->
					<section id="footer">
						<div class="inner">
							<h2 class="major">Контакты</h2>
							<p>Высокие традиции безупречного обслуживания — один из главных факторов, влияющих на любовь и признание постоянных клиентов. Собственный неповторимый стиль оттачивался годами: салон красоты открылся более 18 лет назад, став законодателем стандартов и тенденций в столичной индустрии красоты.</p>
							<form method="post" action="#">
								<div class="fields">
									<div class="field">
										<label for="name">Имя</label>
										<input type="text" name="name" id="name" />
									</div>
									<div class="field">
										<label for="email">Email</label>
										<input type="email" name="email" id="email" />
									</div>
									<div class="field">
										<label for="message">Сообщение</label>
										<textarea name="message" id="message" rows="4"></textarea>
									</div>
								</div>
								<ul class="actions">
									<li><input type="submit" value="Отправить" /></li>
								</ul>
							</form>
							<ul class="contact">
								<li class="fa-home">
									ул. Змитрока Бядули, 13<br />
									г. Минск, Беларусь
								</li>
								<li class="fa-phone">+375 (25) 530-39-23</li>
								<li class="fa-envelope"><a href="#">beatypoint@gmail.com</a></li>
								<li class="fa-twitter"><a href="#">twitter.com/beatypoint</a></li>
								<li class="fa-facebook"><a href="#">facebook.com/beatypoint</a></li>
								<li class="fa-instagram"><a href="#">instagram.com/beatypoint</a></li>
							</ul>
							<ul class="copyright">
								<li>&copy; 2018. Beauty Point. Все права защищены.</li><li>Разработано <a href="https://beone.by">beone studio</a></li>
							</ul>
						</div>
					</section>

			</div>

		<!-- Scripts -->
			<script src="/js/jquery.scrollex.min.js"></script>
			<script src="/js/browser.min.js"></script>
			<script src="/js/breakpoints.min.js"></script>
			<script src="/js/util.js"></script>
			<script src="/js/main.js"></script>
			<script>
				$(document).ready(function () {
					$("#theTarget").skippr();
				});
			</script>
	</body>
</html>