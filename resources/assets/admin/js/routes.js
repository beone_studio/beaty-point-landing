import products from './components/products'
import categories from './components/categories'

export default [
	...categories,
    ...products,
]