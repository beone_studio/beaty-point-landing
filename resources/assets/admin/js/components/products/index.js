import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'
import Layout from './layout.vue'

export default [
    {
        path: '/products',
        component: Layout,
        props: {
            partName: 'Услуги',
            icon: 'library_books',
        },
        children: [ 
            {
                path: '', 
                name: 'list',
                component: Table, 
                props: {
                    partName: 'Список услуг',
                    icon: 'list',
                    showInMenu: true
                }
            },
            {
                path: 'create', 
                name: 'create',
                component: Create,
                props: {
                    partName: 'Добавить услугу',
                    icon: 'add',
                    showInMenu: true
                }                
            },
            {
                path: ':id', 
                name: 'edit',
                component: Edit,
                props: {
                    partName: 'Редактирование услуги',
                    icon: 'edit',
                    showInMenu: false
                }                
            }          
        ]        
    }      
]