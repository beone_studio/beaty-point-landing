import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'
import Layout from './layout.vue'

export default [
    {
        path: '/categories',
        component: Layout,
        props: {
            partName: 'Сферы услуг',
            icon: 'sort',
        },   
        children: [
            {
                path: '', 
                name: 'Список сфер услуг',
                component: Table, 
                props: {
                    partName: 'Список сфер услуг',
                    icon: 'list',
                    showInMenu: true
                },
            },
            {
                path: 'create', 
                name: 'Добавить сферу',
                component: Create,
                props: {
                    partName: 'Добавить сферу',
                    icon: 'add',
                    showInMenu: true
                }                
            },
            {
                path: ':id', 
                name: 'Редактирование сферы',
                component: Edit, 
                props: {
                    partName: 'Редактирование сферы',
                    icon: 'edit',
                    showInMenu: false
                }
            }                   
        ] 
    }      
]